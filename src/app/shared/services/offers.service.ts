import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IOffers } from '../models/offers.interface';

@Injectable()
export class OffersService {

  constructor(private client: HttpClient) { }

  offers(): Observable<IOffers> {
    return this.client.get<IOffers>('/api/offers');
  }
}
