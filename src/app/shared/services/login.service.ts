import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { IUser } from '../models/user.interface';
import { IBasicAuth } from '../models/basic-auth.interface';

@Injectable()
export class LoginService {
  static USER: IUser;
  constructor(private client: HttpClient) { }

  get isLoggedin(): string {
    return sessionStorage.getItem('basicAuth') || localStorage.getItem('basicAuth');
  }

  get users(): Observable<IUser[]> {
    return this.client.get<IUser[]>('/api/users');
  }

  get me(): Observable<IUser> {
    return this.client.post<IUser>('/api/users/me', null, {
      headers: {
        'Authorization': this.isLoggedin,
        'Content-type': 'application/json',
      }
    });
  }

  set me(user: Observable<IUser>) {
    user.subscribe((_user: IUser) => {
      this.user = _user;
    });
  }

  set user(user: IUser) {
    LoginService.USER = user;
  }

  get user(): IUser {
    return LoginService.USER;
  }

  signUp(user: IUser): Observable<IUser> {
    return this.client.post<IUser>('/api/users', { ...user }, { observe: 'body' });
  }

  signIn(auth: { email: string, password: string }, saveMyConnection: boolean = false): Observable<IBasicAuth> {
    const _token = btoa(`${auth.email}:${auth.password}`);
    const _type = 'Basic';
    return of({
      _type,
      _token,
      authorization: `${_type} ${_token}`
    }).pipe(
      tap((basicAuth: IBasicAuth) => {
        this.client.post<IUser>('/api/users/me', null, {
          headers: {
            'Authorization': basicAuth.authorization,
            'Content-type': 'application/json'
          }
        }).subscribe((userInfo: IUser) => {
          this.saveMyConnection(basicAuth.authorization, saveMyConnection);
          this.user = userInfo;
        }, (error) => console.error(error));
      })
    );
  }

  signOut() {
    localStorage.removeItem('basicAuth');
    sessionStorage.removeItem('basicAuth');
  }

  private saveMyConnection(authorization: string, save: boolean = false) {
    if (save) {
      localStorage.setItem('basicAuth', authorization);
    }

    sessionStorage.setItem('basicAuth', authorization);
  }
}
