import { IPhoneNumber } from './phone-number.interface';

export interface IUser {
    name: string;
    firstName?: string;
    lastName: string;
    email: string;
    password: string;
    phoneNumber?: IPhoneNumber;
    _id?: string;
    photoUrl?: string;
    facebookUserId?: string;
}
