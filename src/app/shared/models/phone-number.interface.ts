export interface IPhoneNumber {
    countryCode: string;
    areaCode: string;
    subscriberNumber: string;
}
