import { ICard } from './card.interface';

export interface IOffer extends ICard {
    offerId: number;
    information: string;
    details: string;
    comments: string[];
    tags: string[];
}
