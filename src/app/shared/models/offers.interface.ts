import { IOffer } from './offer.interface';

export interface IOffers extends Array<IOffer> { }
