export interface IBasicAuth {
    _type?: string;
    _token?: string;
    authorization: string;
    _id?: string;
}
