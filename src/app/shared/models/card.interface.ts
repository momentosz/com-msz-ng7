export interface ICard {
    _id: string;
    title: string;
    eatery: string;
    location: string;
    rating: number;
    price: number;
}
