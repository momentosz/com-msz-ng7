import { Component, OnInit, TemplateRef, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgbDropdownConfig, NgbModalConfig, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { takeUntil, tap, catchError } from 'rxjs/operators';
import { Subject, of, from } from 'rxjs';
import { Router } from '@angular/router';
import { IUser } from '../../models/user.interface';
import { AuthService, FacebookLoginProvider, SocialUser } from 'angularx-social-login';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: '[mszHeader]',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [
    NgbDropdownConfig,
    NgbModalConfig,
    NgbModal,
    LoginService,
    AuthService
  ]
})
export class HeaderComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  modal: NgbModalRef;
  signInFormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
  });
  signUpFormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    lastName: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
  });
  destroyer$ = new Subject<boolean>();

  private _isLoggedIn = false;
  private _me: IUser;

  @Output()
  isLoggedInChange = new EventEmitter<boolean>();

  @Input()
  set isLoggedIn(value: boolean) {
    this._isLoggedIn = value;
    this.isLoggedInChange.emit(value);
  }

  get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }

  @Input('userInfo')
  set me(_me: IUser) {
    this._me = _me;
  }

  get me(): IUser {
    return this._me;
  }

  constructor(
    dropdownConfig: NgbDropdownConfig,
    modalConfig: NgbModalConfig,
    private modalService: NgbModal,
    private loginService: LoginService,
    private socialAuth: AuthService,
    private router: Router,
  ) {
    dropdownConfig.placement = ['bottom-right'];
    modalConfig.backdrop = 'static';
    modalConfig.keyboard = false;
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.destroyer$.next(true);
    this.destroyer$.unsubscribe();
  }

  openModal(template: TemplateRef<any>) {
    this.modal = this.modalService.open(template);
  }

  closeModal() {
    this.modal.close();
  }

  signUp() {
    if (this.signUpFormGroup.valid) {
      this.loginService
        .signUp({ ...this.signUpFormGroup.value })
        .pipe(
          takeUntil(this.destroyer$),
        )
        .subscribe(
          data => console.log(data),
          error => console.error(error),
          () => this.closeModal()
        );
    }
  }

  signIn() {
    if (this.signInFormGroup.valid) {
      this.loginService
        .signIn({ ...this.signInFormGroup.value })
        .pipe(
          takeUntil(this.destroyer$),
          tap(data => this.isLoggedIn = data != null),
          catchError(error => of(error))
        )
        .subscribe(
          data => console.log(data),
          error => console.error(error),
          () => this.closeModal()
        );
    }
  }

  signInWithFB() {
    from(this.socialAuth.signIn(FacebookLoginProvider.PROVIDER_ID))
      .pipe(
        takeUntil(this.destroyer$),
        tap(
          (user: SocialUser) =>
            this.loginService
              .signUp({ ...user, password: 'Momentosz@1' })
              .pipe(
                takeUntil(this.destroyer$),
                catchError((error: HttpErrorResponse) => {
                  if (error.status === 412) {
                    debugger;
                    this.loginService
                      .signIn({ ...user, password: 'Momentosz@1' }, true)
                      .pipe(
                        takeUntil(this.destroyer$),
                      )
                      .subscribe();
                    return of(undefined);
                  }
                  return of(error);
                }),
              )
              .subscribe()
        ),
      ).subscribe(
        (user: SocialUser) => this.loginService
          .signIn({ email: user.email, password: 'Momentosz@1' })
          .pipe(
            takeUntil(this.destroyer$),
          ).subscribe(
            data => console.log('success ', data)
          ),
        (error) => console.error(error)
      );
  }

  signOut() {
    this.loginService.signOut();
    this.socialAuth.signOut();
  }

  goToHelp() {
    this.router.navigate(['/help']);
  }

}
