import { Component, OnInit, Input } from '@angular/core';
import { ICard } from '../../models/card.interface';

@Component({
  selector: '[mszCard]',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  private _card: ICard;
  constructor() { }

  @Input()
  set card(_card: ICard) {
    this._card = _card;
  }

  get card(): ICard {
    return this._card;
  }

  imageSrc(eatery: string): string {
    if (eatery) {
      return `/assets/img/restaurantes/${eatery}.png`;
    }
  }

  ngOnInit() { }
}
