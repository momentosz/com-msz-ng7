import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VipComponent } from './vip.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [VipComponent],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [VipComponent],
  bootstrap: [VipComponent]
})
export class VipModule { }
