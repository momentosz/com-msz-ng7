import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagesDetailsComponent } from './images-details.component';

@NgModule({
  declarations: [ImagesDetailsComponent],
  imports: [
    CommonModule
  ], exports: [ImagesDetailsComponent]
})
export class ImagesDetailsModule { }
