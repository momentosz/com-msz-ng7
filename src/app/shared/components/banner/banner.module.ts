import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SelectionModule } from './selection/selection.module';
import { VipModule } from './vip/vip.module';
import { FilterModule } from './filter/filter.module';
import { ImagesDetailsModule } from './images-details/images-details.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    SelectionModule,
    VipModule,
  ],
  exports: [
    SelectionModule,
    VipModule,
    FilterModule,
    ImagesDetailsModule
  ]
})
export class BannerModule { }
