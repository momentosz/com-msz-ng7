import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SelectionComponent } from './selection.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [SelectionComponent],
  imports: [
    CommonModule,
    RouterModule,
  ],
  bootstrap: [SelectionComponent],
  exports: [SelectionComponent]
})
export class SelectionModule { }
