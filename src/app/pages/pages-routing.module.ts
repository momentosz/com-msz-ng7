import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { SelectionComponent } from '../shared/components/banner/selection/selection.component';
import { VipComponent } from '../shared/components/banner/vip/vip.component';
import { FilterComponent } from '../shared/components/banner/filter/filter.component';
import { ImagesDetailsComponent } from '../shared/components/banner/images-details/images-details.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: '',
      children: [
        {
          path: '',
          loadChildren: './home/home.module#HomeModule'
        },
        {
          outlet: 'banner',
          component: SelectionComponent,
          path: ''
        }
      ]
    },
    {
      path: 'vip',
      children: [
        {
          path: '',
          loadChildren: './vip/vip.module#VipModule',
        },
        {
          outlet: 'banner',
          component: VipComponent,
          path: ''
        }
      ]
    },
    {
      path: 'momentosz',
      children: [
        {
          path: '',
          loadChildren: './filter-momentosz/filter-momentosz.module#FilterMomentoszModule'
        },
        {
          outlet: 'banner',
          component: FilterComponent,
          path: ''
        },
      ]
    },
    {
      path: 'selection-sp',
      children: [
        {
          path: '',
          loadChildren: './filter-momentosz/filter-momentosz.module#FilterMomentoszModule'
        },
        {
          outlet: 'banner',
          component: SelectionComponent,
          path: ''
        },
      ]
    },
    {
      path: 'details',
      children: [
        {
          path: '',
          loadChildren: './details/details.module#DetailsModule',
        },
        {
          outlet: 'banner',
          component: ImagesDetailsComponent,
          path: ''
        },
      ]
    },
    {
      path: 'help',
      loadChildren: './help/help.module#HelpModule',
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
