import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VipRoutingModule } from './vip-routing.module';
import { VipComponent } from './vip.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [VipComponent],
  imports: [
    CommonModule,
    VipRoutingModule,
    SharedModule,
  ]
})
export class VipModule { }
