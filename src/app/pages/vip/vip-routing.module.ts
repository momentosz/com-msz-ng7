import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VipComponent } from './vip.component';

const routes: Routes = [{
  path: '',
  component: VipComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VipRoutingModule { }
