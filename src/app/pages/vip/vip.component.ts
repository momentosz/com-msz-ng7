import { Component, OnInit } from '@angular/core';
import { OffersService } from 'src/app/shared/services/offers.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IOffers } from 'src/app/shared/models/offers.interface';

@Component({
  selector: 'msz-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.scss'],
  providers: [OffersService]
})
export class VipComponent implements OnInit {
  private destroy$: Subject<boolean> = new Subject<boolean>();

  offers: IOffers;

  constructor(private offersService: OffersService) { }


  ngOnInit() {
    this.offersService
      .offers()
      .pipe(
        takeUntil(this.destroy$),
      ).subscribe((response: IOffers) => {
        this.offers = response;
      });
  }

}
