import { Component, OnInit, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { LoginService } from '../shared/services/login.service';
import { Observable, Subject } from 'rxjs';
import { IUser } from '../shared/models/user.interface';
import { takeUntil, tap } from 'rxjs/operators';
import { AuthService, SocialUser } from 'angularx-social-login';

@Component({
  selector: 'msz-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss'],
  providers: [LoginService, AuthService]
})
export class PagesComponent implements OnInit, OnChanges, OnDestroy {
  private destroy$: Subject<boolean> = new Subject<boolean>();
  private _me: IUser;

  get isLoggedIn(): boolean {
    return (this.loginService.isLoggedin != null);
  }

  set me(_me: IUser) {
    this._me = _me;
  }

  get me(): IUser {
    return this._me;
  }

  private getMeInfo(): Observable<IUser> {
    return this.loginService
      .me
      .pipe(
        takeUntil(this.destroy$)
      );
  }

  constructor(private loginService: LoginService, private socialAuth: AuthService) { }

  ngOnInit() {
    if (this.isLoggedIn) {
      this.getMeInfo().subscribe(me => this.me = me);
      this.socialAuth
        .authState
        .pipe(
          takeUntil(this.destroy$),
        )
        .subscribe((user: SocialUser) => {
          this.me.email = user.email;
          this.me.name = user.name;
          this.me.firstName = user.firstName;
          this.me.lastName = user.lastName;
          this.me.photoUrl = user.photoUrl;
          this.me.facebookUserId = user.id;
        });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
