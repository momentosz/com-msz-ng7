import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilterMomentoszRoutingModule } from './filter-momentosz-routing.module';
import { FilterMomentoszComponent } from './filter-momentosz.component';

@NgModule({
  declarations: [
    FilterMomentoszComponent
  ],
  imports: [
    CommonModule,
    FilterMomentoszRoutingModule
  ]
})
export class FilterMomentoszModule { }
