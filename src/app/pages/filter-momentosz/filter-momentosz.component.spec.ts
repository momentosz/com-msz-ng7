import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterMomentoszComponent } from './filter-momentosz.component';

describe('FilterMomentoszComponent', () => {
  let component: FilterMomentoszComponent;
  let fixture: ComponentFixture<FilterMomentoszComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterMomentoszComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterMomentoszComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
