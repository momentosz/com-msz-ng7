import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilterMomentoszComponent } from './filter-momentosz.component';

const routes: Routes = [
  {
    path: '',
    component: FilterMomentoszComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilterMomentoszRoutingModule { }
