import { Component } from '@angular/core';

@Component({
  selector: 'msz-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Momentosz';
}
