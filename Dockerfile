# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:11.1.0-alpine as builder
WORKDIR /com-msz-ng7
COPY package*.json /com-msz-ng7/
RUN npm install
COPY ./ /com-msz-ng7/
RUN npm run build -- --prod

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:alpine

COPY --from=builder /com-msz-ng7/dist /usr/share/nginx/html
COPY --from=builder /com-msz-ng7/nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /com-msz-ng7/nginx/momentosz.com.chained.crt /etc/nginx/ssl/momentosz.com.chained.crt
COPY --from=builder /com-msz-ng7/nginx/momentosz.com.key  /etc/nginx/ssl/momentosz.com.key

EXPOSE 80
EXPOSE 443



CMD ["nginx", "-g", "daemon off;"]
